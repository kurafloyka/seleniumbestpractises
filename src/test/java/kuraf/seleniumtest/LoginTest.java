package kuraf.seleniumtest;

import framework.config.Settings;
import kuraf.pages.HomePage;
import kuraf.pages.LoginPage;
import org.junit.Test;


public class LoginTest extends TestInitializeJunit {

    @Test
    public void loginTest() throws InterruptedException {
        LOGGER.info("a test message");
        LOGGER.info("Username : " + excelUtil.ReadCell("UserName", 1)
                + " - Password : " + excelUtil.ReadCell("Password", 1));
        currentPage = GetInstance(LoginPage.class);//new LoginPage();
        currentPage = currentPage.As(LoginPage.class)
                .login(Settings.username, Settings.password);
        currentPage.As(HomePage.class).divaportalAccount();

    }
}
