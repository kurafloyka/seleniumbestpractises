Feature: Login Feature
  This feature is responsible for testing all the scenarios for Login of application

  Scenario: Check Login with username and password
    Given I have navigated to the application
    And I see application opened
    Then I see login page
    When I enter username and password
      | UserName | Password |
      | faruk    | akyol |
    Then I click login button
    Then I should see the branch page