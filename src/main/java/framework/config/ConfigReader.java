package framework.config;

import framework.base.BrowserType;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class ConfigReader {

    private void readProperty() throws IOException {
        try (InputStream input = new FileInputStream("src/main/java/framework/config/GlobalConfig.properties")) {

            Properties prop = new Properties();

            // load a properties file
            prop.load(input);

            Settings.username = prop.getProperty("username");
            Settings.password = prop.getProperty("password");
            Settings.excelPath = prop.getProperty("excelPath");
            Settings.testURL = prop.getProperty("testURL");
            Settings.browserType = BrowserType.valueOf(prop.getProperty("browserType"));


        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    public static void populateSettings() throws IOException {
        ConfigReader configReader = new ConfigReader();
        configReader.readProperty();
    }
}





